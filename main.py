import read_data.format_data as fd

filepath_product_movie = './NONUSER_SAMPLE_CDR/product_movie_20210804_1.txt'
filepath_user = './masked_user_subs/users_20210804_00000001_mask.txt'
filepath_subs = './masked_user_subs/subs_20210804_1_masked.txt'
filepath_user_history = './NONUSER_SAMPLE_CDR/user_history_20210804_01.txt'

dfMovie = fd.format_product_movie(filepath_product_movie)
dfUser = fd.format_user(filepath_user)
dfSubs = fd.format_subs(filepath_subs)
dfUserHistory = fd.format_user_history(filepath_user_history)

print(dfMovie)
