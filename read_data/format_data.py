from datetime import date

import pandas as pd


def formatDate(dateString):
    return date(year=int(dateString[:4]), month=int(dateString[4:6]), day=int(dateString[6:8]))


def format_product_movie(filePath):
    """

    :param filePath: path to data file
    :return: dataframe contains data after formatting
    """
    data = []
    with open(filePath, "r", encoding="utf-8") as f:
        for line in f:
            row = line.split('|')
            row = [x.strip() for x in row]
            row[3] = list(set(row[3].split(',')))
            row[4] = list(set(row[4].split(',')))
            row[5] = int(row[5])
            row[6] = int(row[6])
            row[7] = int(row[7])
            row[8] = list(set(row[8].split(',')))
            row[9] = list(set(row[9].split(',')))
            row[10] = list(set(row[10].split(',')))
            row[11] = list(set(row[11].split(',')))
            row[12] = int(row[12])
            row[13] = int(f'{row[13][:4]}')
            row[14] = float(row[14])
            row[15] = int(row[15])
            data.append(row)
        f.close()

    dfMovie = pd.DataFrame(data=data,
                           columns=['episode_id', 'episode_name', 'episode_description', 'director_id', 'director_name',
                                    'view_count', 'is_series', 'episode_number', 'country_id', 'country_name',
                                    'category_id', 'category_name', 'duration', 'published_time', 'imdb_rate',
                                    'like_count', 'series_id', 'series_name'])
    return dfMovie


def format_user(filePath):
    """

    :param filePath: path to data file
    :return: dataframe contains data after formatting
    """
    data = []
    with open(filePath, "r", encoding="utf-8") as f:
        for line in f:
            row = line.split(',')
            row = [x.strip() for x in row]
            row[3] = formatDate(row[3])
            data.append(row)
        f.close()

    dfUser = pd.DataFrame(data=data, columns=['user_id', 'profile_id', 'phone', 'created_at'])
    return dfUser


def format_subs(filePath):
    """

    :param filePath: path to data file
    :return: dataframe contains data after formatting
    """
    data = []
    with open(filePath, "r", encoding="utf-8") as f:
        for line in f:
            row = line.split(',')
            row = [x.strip() for x in row]
            row[4] = int(row[4])
            row[5] = formatDate(row[5])
            row[6] = formatDate(row[6])
            if row[7] == '':
                row[7] = 0
            else:
                row[7] = formatDate(row[7])
            data.append(row)
        f.close()

    dfSubs = pd.DataFrame(data=data,
                          columns=['user_id', 'msisdn', 'package_id', 'package_name', 'is_active', 'register_time',
                                   'expired_time', 'unreg_time'])
    return dfSubs


def format_user_history(filePath):
    """

    :param filePath: path to data file
    :return: dataframe contains data after formatting
    """
    data = []
    with open(filePath, "r", encoding="utf-8") as f:
        for line in f:
            row = line.split('|')
            row = [x.strip() for x in row]
            row[4] = formatDate(row[4])
            data.append(row)
        f.close()

    dfHistory = pd.DataFrame(data=data,
                             columns=['user_id', 'profile_id', 'item_id', 'item_type', 'time', 'type', 'status'])
    return dfHistory


def format_product_video(filePath):
    """

    :param filePath: path to data file
    :return: dataframe contains data after formatting
    """
    filePath = "./NONUSER_SAMPLE_CDR/product_video_20210804_1.txt"
    data = []
    with open(filePath, "r", encoding="utf-8") as f:
        for line in f:
            row = line.split('|')
            row = [x.strip() for x in row]
            row[3] = int(row[3])
            row[4] = list(set(row[4].split(',')))
            row[5] = list(set(row[5].split(',')))
            row[6] = int(row[6])
            row[7] = formatDate(row[7])
            row[8] = int(row[8])
            data.append(row)
        f.close()
    dfVideo = pd.DataFrame(data=data,
                           columns=['video id', 'video name', 'description', 'view count', 'category id',
                                    'category name',
                                    'duration(second)', 'published_time', 'like_count'])
    return dfVideo